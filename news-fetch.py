#!/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import json
from bs4 import BeautifulSoup
from forex_python.converter import CurrencyRates
from forex_python.bitcoin import BtcConverter

yandex_html = requests.get("https://yandex.ru").text
yandex_soup = BeautifulSoup(yandex_html, 'html.parser')
yandex_news_main = yandex_soup.find('div', {'class': 'content-tabs__items content-tabs__items_active_true'})
yandex_news_region = yandex_soup.find('div', {'class': 'content-tabs__items content-tabs__items_active_false'})

news = []

for a in yandex_news_main.find_all('a'):
    news.append(a.text)
print
for a in yandex_news_region.find_all('a'):
    news.append(a.text)
print

rates = CurrencyRates()
bitcoin = BtcConverter()

currency = {"dollar": "{:.2f}₽".format(rates.get_rate('USD', 'RUB')), 
            "euro": "{:.2f}₽".format(rates.get_rate('EUR', 'RUB')), 
            "bitcoin": "{:.2f}₽".format(bitcoin.get_latest_price('USD'))}

from flask import Flask, render_template
app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html', news=news, currency=currency)
